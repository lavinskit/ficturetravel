<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use \Datetime;

class PhotoController extends AbstractController
{
    public function number()
    {
        // getting the difference between now and my bday
        $my_dbay    = new DateTime('2019-05-07');
        $now        = new DateTime();
        $difference = $my_dbay->diff($now);
        $difference_formated =  $difference->format('%m mouths %d days %H hoours %i minutes and %s seconds');

        return $this->render('photo/photo.html.twig', [
            'countdown' => $difference_formated ,
        ]);
    }
}